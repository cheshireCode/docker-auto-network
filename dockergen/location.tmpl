{{ $CurrentContainer := where $ "ID" .Docker.CurrentContainerID | first }}
{{ range $host, $containers := groupByMulti $ "Env.VIRTUAL_HOST" "," }}

  {{ $default_host := or ($.Env.DEFAULT_HOST) "" }}
  {{ $default_server := index (dict $host "" $default_host "default_server") $host }}

  {{ $container_name := "$CurrentContainer" }}

  {{/* Get the VIRTUAL_PROTO defined by containers w/ the same vhost, falling back to "http" */}}
  {{ $proto := trim (or (first (groupByKeys $containers "Env.VIRTUAL_PROTO")) "http") }}
  {{/* Get the VIRTUAL_ROOT By containers w/ use fastcgi root */}}
  {{ $vhost_root := or (first (groupByKeys $containers "Env.VIRTUAL_ROOT")) "/var/www/public" }}

  {{ $host := trim $host }}
  {{ $is_regexp := hasPrefix "~" $host }}
  {{ $upstream_name := when $is_regexp (sha1 $host) $host }}

  {{ range $container := $containers }}
      {{ $addrLen := len $container.Addresses }}
      {{ range $knownNetwork := $CurrentContainer.Networks }}
          {{ range $containerNetwork := $container.Networks }}
              {{ if (and (ne $containerNetwork.Name "ingress") (or (eq $knownNetwork.Name $containerNetwork.Name) (eq $knownNetwork.Name "host"))) }}
                  ## Can be connected with "{{ $containerNetwork.Name }}" network

                location /{{ $container.Name }}/ {
                    {{ if eq $proto "uwsgi" }}
                      include uwsgi_params;
                      uwsgi_pass {{ trim $proto }}://{{ trim $upstream_name }};
                    {{ else if eq $proto "fastcgi" }}
                      root   {{ trim $vhost_root }};
                      include fastcgi.conf;
                      fastcgi_pass {{ trim $upstream_name }};
                    {{ else }}
                      proxy_pass {{ trim $proto }}://{{ $upstream_name }}/;
                    {{ end }}
                }
              {{ else }}
                  ## Cannot connect to network of this container
                  server 127.0.0.1 down;
              {{ end }}
          {{ end }}
      {{ end }}

  {{ end }}
{{ end }}